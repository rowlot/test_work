##PHP developer test task


 #  Api :
  
  - **[GET /api/categories]**
  - **[GET /api/category/{id}]**
  - **[POST/api/category]**
  - **[PUT /api/category/{id}]**
  - **[DELETE /api/category/{id}]**
  - **[GET /api/search/{name}]**


## U can use how :

```
curl -H 'content-type: application/json' -X GET  "http://*domain*/api/categories"
curl -H 'content-type: application/json' -X GET  "http://*domain*/api/category/2"
curl -H 'content-type: application/json' -X PUT  "http://*domain*/api/category/2" -d '{"name" : "Test_2" , "position" : 1 , "parent_id" : "0"}'
curl -H 'content-type: application/json' -X GET  "http://*domain*/api/search/Test_2"
curl -H 'content-type: application/json' -X POST "http:/*domain*/api/new_category" -d '{"name":"Test_Api","position":"0","parent_id": "1"}'
curl -H 'content-type: application/json' -X DELETE  "http://*domain*/api/category/2"
```

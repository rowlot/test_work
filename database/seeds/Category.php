<?php

use Illuminate\Database\Seeder;

use App\Category as Cat;

class Category extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cat::create(array(
            'name'  => 'Home',
            'position' => '0',
            'parent_id' => '0'
        ));


        Cat::create(array(
            'name'  => 'Test',
            'position' => '0',
            'parent_id' => '0'
        ));

    }
}

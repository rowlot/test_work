<?php

namespace App\Http\Controllers;
use App\Category;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;


class ApiController extends Controller
{


    public function sendMessage($code ,$message){
        return  response()->json(["code"=> $code , "message" => $message ], $code);
    }


    public function index()
    {
        $categories = Category::all();
        if (!$categories) {
            return $this->sendMessage(400 ,'Invalid Data');
        }
        return response()->json([$categories], 200);
    }


    public function show($id)
    {
        if (!$id) {
            return $this->sendMessage(400,'Invalid id');
        }
        $category = Category::find($id);
        return response()->json([$category], 200);
    }

    public function store(Request $request)
    {
        $validate= $this->validate($request ,[
                'name' => 'required | min: 3',
                'position' => 'required',
                'parent_id' => 'required'
        ]);

        if($validate){
            return $this->sendMessage(400 ,"Validator fails");
        }

        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->position = $request->position;
        if ($category->save()) {
            return response()->json([$category] ,200);
        }else
            return $this->sendMessage(400 , "Invalid data");
    }


    public function search($name){
        if(!$name){
           return $this->sendMessage(400, "Nothing not found");
        }
        $result = Category::where('name' ,$name)->get();
        if(count($result) < 1){
         return $this->sendMessage(400 ,"Nothing not found");
        }
        return response()->json($result, 200);
    }


    public function update(Request $request, $id)
    {
        if (!$id) {;
            return $this->sendMessage('400', 'Invalid Id');
        }

        $category = Category::find($id);
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->position = $request->position;

        if ($category->save()) {
            return response()->json([$category] ,200);
        }
        return $this->sendMessage(400,'Invalid data' );
    }


    public function delete($id)
    {
        if (!$id) {
            return $this->sendMessage(400, 'Invalid id');
        }
        $book = Category::find($id);
        $book->delete();
        return $this->sendMessage(200, 'Category deleted');
    }



}

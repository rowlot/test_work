<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class IndexController extends Controller
{

    public function  index(){
        $categories =Category::all();
        return view('category.index' , compact('categories'));
    }


    /*Create new category*/
    public function new_category(){
        $categories = Category::all();
        return view('category.create', compact('categories'));
    }


    /*search function */
    public function search(Request $request){
        $categories = Category::where('name' , $request->category )->get();
        if(count($categories) == 0 ) {
            return  redirect()->back()->with('message','Nothing not found =(');
        }
        return view('category.index', compact('categories'));
    }


    public function edit($id){
        $cur_category = Category::findOrFail($id);
        $categories =Category::all();
        return view('category.edit', compact('cur_category' , 'categories'));
    }


    public function update(Request $request){
        $this->validate($request, [
            'name' => 'required | min: 3',
            'position' => 'required',
            'parent_id' => 'required'
        ]);
        Category::findOrFail($request->id)->update($request->all());
        return redirect()->route('category')->with('message', 'Category updated');
    }



    public function save(Request $request){
         $this->validate($request, [
                'name' => 'required | min: 3',
                'position' => 'required',
                'parent_id' => 'required'
            ]);
        
        

        $category = new Category();
        $category->name = $request->name;
        $category->parent_id = $request->parent_id;
        $category->position = $request->position;
        $category->save();
        return redirect()->route('category')
            ->with('message','New category created successfully');
    }

    public function delete(Request $request){

        Category::find($request->id)->delete();
        return redirect()->route('category')
            ->with('message','Category deleted successfully');

    }
    

}

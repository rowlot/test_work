<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UploadController extends Controller
{

    public $coll;

    public function index(){

        return view('file.index', compact($this->coll));
    }

    public function upload(Request $request){

        // i dont save file - open from /tmp..
        foreach ($request->file() as $file){
            foreach ($file as $node) {
                $f =fopen($node->path() , 'r');
                $ln=0;
                while ($line = fgets($f) ){
                    $str = explode('|', $line );
                    // easy exception
                    if( !isset($str[0])| !isset($str[1]) | !isset($str[2])){
                        return redirect()->back()->with('error' , 'Oops ... not found column =(' );
                    }
                    $this->coll[++$ln] = [ 'node_id' => $str[0], 'parent_id' => $str[1] , 'node_name' => $str[2] ];

                }

                fclose($f);
            }
        }
        $coll = $this->coll;
        return view('file.index' , compact('coll'));
    }
}

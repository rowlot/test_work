<?php

namespace App\Helper;
use App;


class Tree_as_array{

    public $coll = array();

    function __construct($array){
         $this->coll = $array;
    }

    public function getChild($id){
        $array = array();
        $i = 0;
        foreach ($this->coll as $item ){
            if($item['parent_id'] == $id ){
                $array[$i++] = $item;
            }
        }
        return $array;
    }

    public function getPadding($lvl){
        $str = '';
        for($i= 1 ; $i < $lvl; $i++ ){
            $str .='-';
        }
        return $str;
    }

    public function tree_show( $ParentId , $lvl){
        global $lvl;
        $lvl++;
        $child = $this->getChild($ParentId);
        if(count($child) > 0){
            foreach ($child as $a){
                echo '<p>'. $this->getPadding($lvl) .' '.$a['node_name'] .'('. $a['node_id'].')</p>';
                $this->tree_show( $a['node_id'] , $lvl);
                $lvl--;
            }
        }
    }


}
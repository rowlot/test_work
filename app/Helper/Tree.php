<?php

namespace App\Helper;
use App;
use App\Category;


class Tree{
    
    static function tree($ParentId , $lvl){
        global $lvl;
        $lvl++;
        $child = Category::where('parent_id' , $ParentId)->get();
        if(count($child) > 0){
            $child->sortBy('position');
            echo '<ul>';
            foreach ($child as $a){
                echo '<li>'. $a->name . '(' . $a->position.')</li>';
                self::tree( $a->id , $lvl);
                $lvl--;
            }
            echo '</ul>';
        }
    }
    
    
    
    
    

}


?>


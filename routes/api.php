<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


    Route::get('/api/categories', 'ApiController@index');
    Route::get('/api/category/{id}', 'ApiController@show');
    Route::post('/api/category', 'ApiController@store');
    Route::put('/api/category/{id}', 'ApiController@update');
    Route::delete('/api/category/{id}', 'ApiController@delete');
    Route::get('/api/search/{name}', 'ApiController@search');

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('category');
Route::get('/category/{id}', 'IndexController@show');
Route::get('/edit/{id}', 'IndexController@edit');
Route::get('/new_category', 'IndexController@new_category');
Route::get('show_as_tree', function () {
    return view('category.as_tree');
});

Route::post('/save', 'IndexController@save');
Route::post('/search', 'IndexController@search');
Route::post('/update', 'IndexController@update');
Route::post('/delete', 'IndexController@delete');


Route::get('upload', 'UploadController@index')->name('upload');
Route::post('upload', 'UploadController@upload');


Route::get('/api/categories', 'ApiController@index');
Route::get('/api/category/{id}', 'ApiController@show');
Route::post('/api/new_category', 'ApiController@store');
Route::put('/api/category/{id}', 'ApiController@update');
Route::get('/api/search/{name}', 'ApiController@search');
Route::delete('/api/category/{id}', 'ApiController@delete');
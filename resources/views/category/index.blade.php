@extends('welcome')


@section('content')
    <div class="container">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                <tr>
                    <td class="text-center">Name</td>
                    <td class="text-center">Position</td>
                    <td class="text-center">Action</td>
                </tr>

                </thead>
                @foreach($categories as $category)
                    <tr>
                        <td class="text-center">{{ $category->name }}</td>
                        <td class="text-center">
                            {{ $category->position }}
                        </td>
                        <td>
                            <div class="button-group text-center">
                                <a href="{{ url('edit/'. $category->id) }}" class="btn btn-primary"
                                   style="display: inline">Edit</a>
                                <form method="post" action="{{ url('delete') }}" style="display:inline;">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    <input type="hidden" name="id" value="{{ $category->id }}"/>
                                    <button type="submit">Delete</button>
                                </form>
                            </div>
                        </td>
                @endforeach
            </table>
        </div>
</div>

@endsection
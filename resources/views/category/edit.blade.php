@extends('welcome')

@section('content')
    <div class="container">
        <form class="form-horizontal" method="post" action="{{ url('update') }}">
            <h3 class="text-center">Edit category {{ $cur_category->name }}</h3>
            <!-- Text input-->
            <div class="form-group row">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-4">
                    <input name="name" type="text" value="{{ $cur_category->name }}" class="form-control input-md">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label" for="parent_id">Select parent</label>
                <div class="col-md-4">
                    <select name="parent_id">
                        <option value="0">No parent</option>
                        @foreach($categories as $category)
                            @if($category->id !=  $cur_category->id)
                                <option value="{{ $category->id }}" <?php if ($cur_category->parent_id == $category->id) echo "selected"; ?> >{{ $category->name }}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>


            <div class="form-group row">
                <label class="col-md-4 control-label" for="position">Position</label>
                <div class="col-md-4">
                    <input type="number" name="position" value="{{ $cur_category->position }}"/>
                </div>
            </div>

            <!--hidden _token-->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="hidden" name="id" value="{{ $cur_category->id }}">

            <div class="form-group row">
                <div class="offset-md-4 col-md-8">
                    <div class="button-group pull-right">
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>

        </form>
    </div><!--container-->




@endsection
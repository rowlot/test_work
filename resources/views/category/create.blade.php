@extends('welcome')

@section('content')
    <div class="container">
        <form class="form-horizontal" method="post" action="{{ url('save') }}">
            <h3 class="text-center">New Category</h3>
            <!-- Text input-->
            <div class="form-group row">
                <label class="col-md-4 control-label" for="name">Name</label>
                <div class="col-md-4">
                    <input name="name" type="text" class="form-control input-md">
                </div>
            </div>
            <div class="form-group row">
                <label class="col-md-4 control-label" for="parent_id">Select parent</label>
                <div class="col-md-4">
                    <select name="parent_id">
                        <option value="0">No parent</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>

                </div>
            </div>


            <div class="form-group row">
                <label class="col-md-4 control-label" for="position">Position</label>
                <div class="col-md-4">
                    <input type="number" name="position" value="0"/>
                </div>
            </div>

            <!--hidden _token-->
            <input type="hidden" name="_token" value="{{ csrf_token() }}">

            <div class="form-group row">
                <div class="offset-md-4 col-md-8">
                    <div class="button-group pull-right">
                        <a href="{{ url()->previous() }}" class="btn btn-primary">Cancel</a>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </div>
            </div>

        </form>
    </div><!--container-->




@endsection
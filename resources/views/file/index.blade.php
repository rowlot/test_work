@extends('welcome')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <h2 class="text-center">Part 1</h2>
                <h3>Main PHP functionality</h3>
                <p>We have a text file with structured tree information in form of:</p>
                <pre>node_id | parent_id | node_name</pre>
                <br>
                <p>Where:</p>
                <p>Node_id - numeric node ID</p>
                <p>Parent_id - ID of the parent node</p>
                <p>Node_name - category/node name</p>
                <br>
                <p>The task is to represent this tree with correct paddings for every level</p>
                <ul>
                    <li>level one zero padding</li>
                    <li>Level two one padding</li>
                    <li>Etc...</li>
                </ul>
                <h3>Input data:</h3>
                <pre>
                12|11|20D
                11|9|Canon
                ...
                </pre>
            </div><!--col-sm-6-->

            <div class="col-sm-6">
                @if (session()->has('error'))
                    <div class="alert alert-danger">
                        <ul>
                            <li>{{ session()->get('error') }}</li>
                        </ul>
                    </div>
                @endif


                <p>Input file:</p>
                <form action="{{ url('upload') }}" method="post" enctype="multipart/form-data" >
                    <input type="file" name="text_file[]" >
                    <input name="_token" hidden value="{{ csrf_token() }}">
                    <button class="right" type="submit">Upload</button>
                </form>
                    <div class="row">

                        <div class="box" style="padding: 40px 10px; ">

                        @if(isset($coll) != null)
                        <?php
                         $var = new Helper_array($coll);
                         $var->tree_show(0,0);
                         ?>

                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
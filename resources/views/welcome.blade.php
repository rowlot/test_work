<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token()}}">
    <title>{{ config('app.title') }}</title>
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <style>
        .p5 {
            padding: 5px;
        }

        #as_tree {
            color: red;
        }

    </style>
</head>
<body>
<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">{{ config('app.name') }}</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li><a href="{{ url('/') }}">Categories</a>
            <li>
        </ul>
        <div class="col-sm-3 col-md-3">
            <form class="navbar-form" method="post" action="{{ url('/search') }}" role="search">
                <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search" name="category">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>

        <ul class="nav navbar-nav">
            <li><a href="{{ url('/show_as_tree') }}" id="as_tree">Show as tree</a></li>
            <li><a href="{{ url('/upload') }}">Path 1 (Upload file)</a></li>
        </ul>

        <div class="nav navbar-nav navbar-right p5">
            <a class="btn btn-primary" href="{{ url('/new_category') }}">New Category</a>
        </div>
    </div><!-- /.navbar-collapse -->
</nav>


<div class="row">
    <div class="container">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif


        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div><!--container-->
</div>

@yield('content')

<script src="{{ mix('js/app.js') }}"></script>
@yield('script')
</body>

</html>